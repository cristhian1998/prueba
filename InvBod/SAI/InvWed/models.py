from django.db import models

# Create your models here.

class Registro_recurso(models.Model):
    categoria = models.CharField(max_length=10, primary_key=True)
    codigo = models.CharField(max_length=5)
    nombre= models.CharField(max_length=50)
    marca = models.CharField(max_length=5)
    serie = models.CharField(max_length=5)

    def __str__(self):
        return f'Registro_recurso{self.categoria}: {self.codigo}: {self.nombre}: {self.marca}: {self.serie}'

class Registro_persona(models.Model):
    identificacion = models.CharField(max_length=5, primary_key=True)
    nombre = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    id_registro = models.ForeignKey(Registro_recurso, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return f'Registro_persona {self.identificacion}: {self.nombre}: {self.apellidos}'
