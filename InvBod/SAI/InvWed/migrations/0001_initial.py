# Generated by Django 3.2.6 on 2021-08-24 06:30

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Registro_persona',
            fields=[
                ('identificacion', models.CharField(max_length=5, primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=5)),
                ('apellidos', models.CharField(max_length=5)),
            ],
        ),
    ]
