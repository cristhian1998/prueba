from django.forms import modelform_factory
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.
from InvWed.models import Registro_persona, Registro_recurso


def bienvenido(request) :

    no_personas_var = Registro_persona.objects.count ()
    personas = Registro_persona.objects.all ()
    return render (request, 'inicio.html', {'no_personas' : no_personas_var, 'personas' : personas})


def detallepersona(request, identificacion) :
    # persona = Registro_persona.objects.get(pk=identificacion)
    persona = get_object_or_404 (Registro_persona, pk=identificacion)
    return render (request, 'detalle.html', {'persona' : persona})



PersonaForm = modelform_factory (Registro_persona, exclude=[])


def nuevapersona(request) :
    if request.method == 'POST' :
        formapersona = PersonaForm (request.POST)
        if formapersona.is_valid () :
            formapersona.save ()
            return redirect ('index')
    else :
        formapersona = PersonaForm ()
    return render (request, 'Nueva.html', {'formapersona' : formapersona})


def editarpersona(request, identificacion) :
    if request.method == 'POST' :
        formapersona = PersonaForm (request.POST, instance=Registro_persona)
        if formapersona.is_valid () :
            formapersona.save ()
            return redirect ('index')
    else :
        persona = get_object_or_404 (Registro_persona, pk=identificacion)
        formapersona = PersonaForm (intance=Registro_persona)
    return render (request, 'editar.html', {'formapersona' : formapersona})


def eliminarpersona(request, identificacion) :
    persona = get_object_or_404 (Registro_persona, pk=identificacion)
    if persona :
        persona.delete ()
    return redirect ('index')


PersonaForm1 = modelform_factory (Registro_recurso, exclude=[])


def nuevorecurso(request) :
    if request.method == 'POST' :
        formapersona = PersonaForm1 (request.POST)
        if formapersona.is_valid () :
            formapersona.save ()
            return redirect ('index')
    else :
        formapersona = PersonaForm1 ()
    return render (request, 'recurso.html', {'formapersona' : formapersona})
