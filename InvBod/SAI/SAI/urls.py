"""SAI URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from InvWed.views import bienvenido, detallepersona, nuevapersona, editarpersona, nuevorecurso, eliminarpersona

urlpatterns = [
    path('admin/', admin.site.urls),
    #path('bienvenido/', bienvenido)
    path('', bienvenido,name='index'),
    path('Detalle_persona/<int:identificacion>', detallepersona),
    path('nueva_persona', nuevapersona),
    path('Editar_persona/<int:identificacion>', editarpersona),
    path('nuevo_Recurso', nuevorecurso),
    path('Eliminar_persona/<int:identificacion>', eliminarpersona),

]
